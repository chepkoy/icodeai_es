import setuptools

setuptools.setup(
    name="icodeai_es",
    version="0.0.0",
    description ="Elastic Search",
    packages = setuptools.find_packages('src'),
    package_dir={'':'src'},
    author="Allan Chepkoy",
    author_email = "allankiplangat22@gmail.com",
    licence="",
    install_requires=['']
)